// +build appengine

package api

import (
	"net/http"

	"golang.org/x/net/context"

	"github.com/drewwells/fitkiq/store"
	"github.com/drewwells/fitkiq/store/adapter"
	"github.com/drewwells/fitkiq/types"

	"google.golang.org/appengine"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/urlfetch"
)

func init() {
	getHTTPCli = func(r *http.Request) *http.Client {
		ctx := appengine.NewContext(r)
		log.Infof(ctx, "Creating appengine context: % #v\n", ctx)

		cli := &http.Client{
			Transport: &urlfetch.Transport{
				Context: ctx,
				AllowInvalidServerCertificate: true,
			}}
		log.Infof(ctx, "Middleware executed: % #v", cli)
		return cli
	}

	FromRequest = func(r *http.Request) *store.DB {
		ctx := types.NewContext(context.TODO(), r)
		s, err := adapter.NewStore(ctx, "")
		if err != nil {
			log.Errorf(ctx, "failed to start store:", err)
			return nil
		}
		return store.NewDB(s)
	}

	IsDev = func() bool {
		return appengine.IsDevAppServer()
	}

}
