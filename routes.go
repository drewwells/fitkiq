package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"

	"golang.org/x/net/context"

	"github.com/codegangsta/negroni"
	"github.com/drewwells/fitkiq/facade"
	"github.com/drewwells/fitkiq/schedule"
	"github.com/drewwells/fitkiq/store"
	"github.com/drewwells/fitkiq/store/adapter"
	"github.com/drewwells/fitkiq/types"
	"github.com/drewwells/fitkiq/versionpkg"
	"github.com/drewwells/makeplans"
	"github.com/gorilla/mux"
)

// Some things to toggle behavior between appengine and native go
var (
	getHTTPCli  func(r *http.Request) *http.Client
	IsDev       func() bool
	FromRequest func(r *http.Request) *store.DB
	dbPath      = "bolt.db"

	mw []negroni.Handler
)

var (
	userRoutes map[string]string
)

var trainerRoutes = map[string]string{
	"TRAINER":         "/trainers/{id}",
	"TRAINERSERVICES": "/trainers/{id}/services/",
	"TRAINERSERVICE":  "/trainers/{id}/services/{service_id}",
	"TRAINERPROVS":    "/trainers/providers/",
}

var routes = map[string]string{
	"LOGIN":       "/login",
	"SERVICES":    "/services/",
	"SERVICESLOT": "/services/{id}/slots/",
	"SIGNUP":      "/signup",
	"TRAINERS":    "/trainers/",
	"VERSION":     "/version",
	"FB":          "/fblogin",
}

var adminRoutes = map[string]string{
	"BOOKING":   "/bookings/{id}",
	"BOOKINGS":  "/bookings/",
	"PROVIDER":  "/provider/{id}",
	"PROVIDERS": "/providers/",
	"RESOURCE":  "/resources/{id}",
	"RESOURCES": "/resources/",
	"SERVICE":   "/services/{id}",
	"SERVICES":  "/services/",
	"TRAINERS":  "/trainers/",
	"USERS":     "/users/",
	"USER":      "/users/{user_id}",
}

func init() {
	getHTTPCli = func(r *http.Request) *http.Client {
		return nil
	}

	userRoutes = map[string]string{
		"BOOKING":  "/bookings/{id}",
		"BOOKINGS": "/bookings/",
		"PROFILE":  "/profile",
	}

	FromRequest = func(r *http.Request) *store.DB {
		s, err := adapter.NewStore(context.TODO(), dbPath)
		if err != nil {
			log.Println("failed to start store:", err)
			return nil
		}
		return store.NewDB(s)
	}
}

type Server struct {
	sch *schedule.Schedule
}

// New starts a mux router and binds to default http ServerMux
func New(file string) (*Server, error) {
	sch, err := schedule.New(file)
	if err != nil {
		return nil, err
	}
	s := &Server{
		sch: sch,
	}

	root := mux.NewRouter()
	root.Handle("/", http.RedirectHandler("/v1/", http.StatusMovedPermanently))
	root.Handle("/v1", http.RedirectHandler("/v1/", http.StatusMovedPermanently))
	// swagger UI
	root.PathPrefix("/swagger/").Handler(http.FileServer(http.Dir("gae/public")))
	// Route traffic to ui
	root.PathPrefix("/ui/").Handler(http.StripPrefix("/ui/", http.FileServer(http.Dir("ui/www"))))

	r := mux.NewRouter().PathPrefix("/v1").Subrouter()
	// r.Handle("/ui", http.StripPrefix("/ui", http.FileServer(http.Dir("ui/www"))))
	// Swagger endpoints
	r.Handle("/swagger.json",
		http.StripPrefix("/v1/", http.FileServer(http.Dir("gae/"))))

	r.HandleFunc("/", handler(routes, "/v1"))
	r.HandleFunc("/userroutes", handler(userRoutes, "/v1"))
	r.HandleFunc(routes["LOGIN"], s.login).Methods("POST", "OPTIONS")
	r.HandleFunc(routes["VERSION"], version)
	r.HandleFunc(routes["SERVICES"], s.services)
	r.HandleFunc(routes["SIGNUP"], s.Signup).Methods("POST")
	r.HandleFunc(routes["SERVICESLOT"], s.ServiceSlot).Methods("GET")
	r.HandleFunc(routes["TRAINERS"], s.listTrainers).Methods("GET")
	r.HandleFunc(routes["FB"], s.checkFB).Methods("POST")

	// User contains routes that require user level authentication
	user := mux.NewRouter().PathPrefix("/v1").Subrouter()
	user.HandleFunc(userRoutes["BOOKING"], s.userbooking).Methods("GET")
	user.HandleFunc(userRoutes["BOOKING"], s.userbookingupdate).Methods("PUT")
	user.HandleFunc(userRoutes["BOOKING"], s.userbookingdelete).Methods("DELETE")
	user.HandleFunc(userRoutes["BOOKINGS"], s.userbookings).Methods("GET")
	user.HandleFunc(userRoutes["BOOKINGS"], s.makeBooking).Methods("POST")
	user.HandleFunc(userRoutes["PROFILE"], s.userprofile).Methods("GET")

	trainer := mux.NewRouter().PathPrefix("/v1").Subrouter()
	trainer.HandleFunc(trainerRoutes["TRAINER"], s.TrainerUpdate).Methods("PUT")
	trainer.HandleFunc(trainerRoutes["TRAINER"], s.trainer)
	trainer.HandleFunc(trainerRoutes["TRAINERSERVICES"], s.trainerServices)
	trainer.HandleFunc(trainerRoutes["TRAINERSERVICE"], s.trainerService)
	trainer.HandleFunc(trainerRoutes["TRAINERPROVS"], s.trainerProviders)

	// Admin contains routes that only admin user should be able to access
	admin := mux.NewRouter().PathPrefix("/admin").Subrouter()
	admin.HandleFunc("/", handler(adminRoutes, "/admin"))
	admin.HandleFunc(adminRoutes["BOOKINGS"], s.bookings).Methods("GET")
	admin.HandleFunc(adminRoutes["BOOKINGS"], s.adminMakeBooking).Methods("POST")
	admin.HandleFunc(adminRoutes["BOOKING"], s.bookingDelete).Methods("DELETE")
	admin.HandleFunc(adminRoutes["PROVIDERS"], s.providers).Methods("GET")
	admin.HandleFunc(adminRoutes["PROVIDERS"], s.provider).Methods("POST")
	admin.HandleFunc(adminRoutes["PROVIDER"], s.providerMutate).Methods("DELETE")
	admin.HandleFunc(adminRoutes["RESOURCES"], s.resources).Methods("GET")
	admin.HandleFunc(adminRoutes["RESOURCE"], s.resourceDelete).Methods("DELETE")
	admin.HandleFunc(adminRoutes["RESOURCE"], s.resourceUpdate).Methods("PUT")

	admin.HandleFunc(adminRoutes["SERVICES"], s.service).Methods("POST")
	admin.HandleFunc(adminRoutes["SERVICE"], s.serviceMutate).Methods("PUT", "DELETE")
	admin.HandleFunc(adminRoutes["TRAINERS"], s.Trainers).Methods("GET")

	admin.HandleFunc(adminRoutes["USERS"], s.adminusers)
	admin.HandleFunc(adminRoutes["USER"], s.adminuser)

	trainer.NewRoute().Handler(negroni.New(
		negroni.HandlerFunc(mwadmin),
		negroni.Wrap(admin),
	))

	user.NewRoute().Handler(negroni.New(
		negroni.HandlerFunc(mwtrainer),
		negroni.Wrap(trainer),
	))

	r.NewRoute().Handler(negroni.New(
		negroni.HandlerFunc(s.token),
		negroni.Wrap(user),
	))

	root.NewRoute().Handler(negroni.New(
		negroni.HandlerFunc(cors),
		negroni.HandlerFunc(s.setupCli),
		negroni.Wrap(r),
	))

	root.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		isErrBadRequest(w, r, errors.New("404 not found"))
	})

	http.Handle("/", root)
	return s, nil
}

func (s *Server) services(w http.ResponseWriter, r *http.Request) {
	svcs, err := s.sch.Services()
	if err != nil {
		httpError(w, r, err.Error(), 500)
		return
	}
	marshal(w, r, svcs)
}

// ServiceSlot swagger:route GET /v1/services/{id}/slots/ schedule ServiceSlot
// Gets available time slots for the requested Service
//
// Gets all the time slots
//
//
// Consumes:
// application/json
//
// Schemes: http, https
//
// Produces:
// application/json
//
// Responses:
//       200: SlotResponse
func (s *Server) ServiceSlot(w http.ResponseWriter, r *http.Request) {
	sid := mux.Vars(r)["id"]
	if len(sid) == 0 {
		httpError(w, r, "id missing", http.StatusBadRequest)
		return
	}

	id, err := strconv.Atoi(sid)
	if isErrBadRequest(w, r, err) {
		return
	}
	m := r.URL.Query()
	var from, to time.Time
	layout := "2006-01-02"
	if len(m["from"]) > 0 {
		from, _ = time.Parse(layout, m["from"][0])
	}

	if len(m["to"]) > 0 {
		to, _ = time.Parse(layout, m["to"][0])
	}

	var resources []int
	if len(m["res"]) > 0 {
		if len(m["res"][0]) > 0 {
			for _, sid := range strings.Split(m["res"][0], ",") {
				id, err := strconv.Atoi(sid)
				if err != nil {
					continue
				}
				resources = append(resources, id)
			}
		}
	}

	params := makeplans.SlotParams{
		From:              from,
		To:                to,
		SelectedResources: resources,
	}

	slots, err := s.sch.ServiceSlot(id, params)
	if isErrInternal(w, r, err) {
		return
	}

	marshal(w, r, slots)
}

// Signup swagger:route POST /v1/signup user Signup
//
// Signup a new user or trainer
//
// Consumes:
// application/json
//
// Schemes: http, https
//
// Produces:
// application/json
//
// Responses:
// 200: UserResponse
func (s *Server) Signup(w http.ResponseWriter, r *http.Request) {
	ctx := context.TODO()
	db := FromRequest(r)

	bs, err := ioutil.ReadAll(r.Body)
	if err != nil {
		httpError(w, r, "Invalid request body", http.StatusBadRequest)
		return
	}
	var u types.User
	err = json.Unmarshal(bs, &u)
	if err != nil {
		httpError(w, r, err.Error(), http.StatusBadRequest)
		return
	}
	// If user is a trainer, create a default resource for them
	// and stick the resourceID in the user
	if u.Role == types.TrainerRole {
		err = s.makeTrainerResource(&u)
		// Assume default case is Member
	} else { //if u.Role == types.MemberRole {
		u.Role = types.MemberRole
		err = s.makePerson(&u)
	}
	if err != nil {
		httpError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}

	user, err := db.AddUser(ctx, u)
	if err != nil {
		httpError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}
	user.Password = ""

	marshal(w, r, types.UserResponse{User: user})
}

func (s *Server) makePerson(u *types.User) error {
	p := makeplans.Person{
		Name: u.FirstName + " " + u.LastName,
	}
	pp, err := s.sch.MakePerson(p)
	if err != nil {
		return err
	}
	u.PersonID = pp.ID
	return nil
}

// makeTrainerResource creates a resource for the trainer
// and mutates the user to inject the resource ID in it
func (s *Server) makeTrainerResource(u *types.User) error {
	r, err := s.sch.MakeResource(makeplans.Resource{
		Title:    u.FirstName + " " + u.LastName,
		Capacity: 1,
	})
	if err != nil {
		return err
	}
	if r.ID == 0 {
		log.Printf("% #v\n", r)
		return errors.New("invalid resource ID")
	}
	u.ResourceID = r.ID
	return nil
}

func PrintRoutes(w http.ResponseWriter, rts map[string]string, prefix string) {
	w.Header().Add("Content-Type", "text/html")
	sorted := make([]string, 0, len(rts))
	for _, r := range rts {
		sorted = append(sorted, r)
	}
	sort.Sort(sort.StringSlice(sorted))
	rt := routeTemplate{
		Prefix: prefix,
		Links:  sorted,
	}
	err := tpls.ExecuteTemplate(w, "printroutes", rt)
	if err != nil {
		fmt.Println(err)
	}
}

func handler(m map[string]string, prefix string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		PrintRoutes(w, m, prefix)
	}
}

func version(w http.ResponseWriter, r *http.Request) {
	marshal(w, r, versionpkg.Info())
}

// ListTrainers swagger:route GET /v1/trainers/ user User
// Retrieves the public list of trainers
//
//
// Consumes:
// application/json
//
// Schemes: http, https
//
// Produces:
// application/json
//
// Responses:
//       200: UserResponse
func (s *Server) listTrainers(w http.ResponseWriter, r *http.Request) {

	db := FromRequest(r)
	ts, err := facade.Trainers(db, s.sch, true)
	if err != nil {
		httpError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}
	marshal(w, r, ts)
}
