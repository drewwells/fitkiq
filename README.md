### What is this repository for? ###

* API related operations

### How do I get set up? ###

Download [Go](http://golang.org/dl)

Run `make`

#### OPTIONAL

Download [Go Appengine SDK](https://cloud.google.com/appengine/downloads?hl=en)

Add goapp to your path. To test appengine locally, run

`make serve`
