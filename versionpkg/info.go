package versionpkg

import "fmt"

var (
	Commit  string
	Version string
	Branch  string
)

func Info() Build {
	return Build{
		Commit, Version, Branch,
	}
}

type Build struct {
	Commit  string
	Version string
	Branch  string
}

func (b Build) String() string {
	return fmt.Sprintf("%s-%s", b.Version, b.Commit)
}
