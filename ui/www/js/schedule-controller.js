var schmod = angular.module('fitkiq.schedule', [
  'fitkiq.workout',
  'fitkiq.trainer',
  'ionic-datepicker'
])

schmod.controller('ScheduleCtrl', function($scope, DataService, workoutService, $state) {

  var vm = this;

  vm.workoutType = {};
  //Page loads with "group" selected, set it as selected value by default
  vm.workoutType.Selected = "group";
  vm.selectedWorkout = {};
  vm.isWorkoutSelected = false;

  workoutService.all().then(function(workouts) {
    vm.workouts = workouts;
  }, function() {
    // Refetch?
    console.log('reject', arguments);
  })

  vm.ChooseType = function(obj) {
    var dataValue = obj.target.attributes.data.value,
        activeCurrent = document.getElementsByClassName('active group');

    while(activeCurrent[0] && !obj.target.classList.contains('active')) {
      activeCurrent[0].classList.remove('active');
      obj.target.classList.add('active');
      vm.workoutType.Selected = dataValue;
    }
  };

  vm.ChooseWorkout = function(obj) {
    var dataValue = obj.target.attributes.data.value;
    obj.target.classList.toggle('active');

    [].map.call(document.querySelectorAll('.workout'), function(el) {
      if (obj.target.attributes.data.value !== el.attributes.data.value) {
        el.classList.remove('active');
      }
    });

    if (obj.target.classList.contains('active')) {
      vm.selectedWorkout.Value = dataValue;
      vm.isWorkoutSelected = true;

    } else {
      vm.selectedWorkout.Selected = "";
      vm.isWorkoutSelected = false;
    }
    console.log(vm.selectedWorkout.Value.length);

  };

  vm.SubmitWorkout = function() {

    console.log('submit selected', vm.selectedWorkout);
    console.log('submit type', vm.workoutType);
    $state.go('tab.workout-schedule', { workoutType: vm.workoutType.Selected, workout: vm.selectedWorkout.Value });
  };
});
