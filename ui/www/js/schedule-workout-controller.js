angular.module('fitkiq.schedule')
  .controller('ScheduleWorkoutCtrl', function(
    $scope,
    DataService,
    $state,
    $stateParams,
    $filter,
    trainerService
  ) {

    var vm = this;

    vm.selected = {
      date: "Pick your date",
      time: "Pick your time",
      dateSelected: false,
      timeSelected: false
    };

    function addDays(date, days) {
      var res = new Date(date);
      res.setDate(res.getDate() + days);
      return res;
    }

    var tomorrow = addDays(new Date(), 1);
    tomorrow.setHours(18);
    tomorrow.setMinutes(0);
    tomorrow.setSeconds(0);
    tomorrow.setMilliseconds(0);
    vm.selected.date = tomorrow;
    vm.pickDate = function() {

      function onSuccess(date) {
        var selected = new Date(date);
        vm.selected.date = (selected.getMonth() + 1) + "-" + selected.getDate() + "-" + selected.getFullYear();
        vm.selected.selectedDate = true;
        //TODO: Should revisit this later to remove the $scope.$apply
        $scope.$apply(vm.selected.date);
      }
      if (window.datePicker) {
        datePicker.show(DataService.GetDateTimeOptions("date"), onSuccess, onError);
      }
    };

    function onError(error) { // Android only
      alert('Error: ' + error);
    }

    vm.pickTime = function() {
      //options = DataService.GetDateTimeOptions("time");
      function onTimeSuccess(date) {
        var selectedTime = new Date(date);
        vm.selected.time = selectedTime.getHours() + ":" + selectedTime.getMinutes();
        vm.selected.selectedTime = true;
        //this feels wrong to me but is the only way I could get it to return the date selected on first go-round
        //might need to revisit later
        $scope.$apply(vm.selected.time);
      }

      if (window.datepicker) {
        datePicker.show(DataService.GetDateTimeOptions("time"), onTimeSuccess, onError);
      }
    }

    vm.workout = DataService.GetWorkouts().get($stateParams.workout);

    vm.SubmitWorkoutTimes = function() {

      var from = $filter('date')(vm.selected.date, 'yyyy-MM-dd');
      var to = $filter('date')(addDays(vm.selected.date, 1), 'yyyy-MM-dd');

      $state.go('workout-location', {
        workoutType: $stateParams.workoutType,
        workout: $stateParams.workout,
        from: from,
        to: to
      });
    }
  });
