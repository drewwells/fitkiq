angular.module('fitkiq')
    .factory('DataService', function() {
        var _service = {
            GetWorkouts: function() {
                var workouts = [
                    { id: 0, name: 'Yoga Session', icon:'ion-leaf' },
                    { id: 1, name: 'Strength Training', icon:'ion-ios-body' },
                    { id: 2, name: 'Weight Loss', icon:'ion-android-walk' },
                    { id: 3, name: 'General Fitness', icon:'ion-android-bicycle' },
                    { id: 4, name: 'Specialty Training', icon:'ion-ios-pint' }
                    ];
                    return {
                        all: function() {
                            return workouts;
                        },
                        get: function(workoutId) {
                            for (var i = 0; i < workouts.length; i++) {
                                if (workouts[i].id === parseInt(workoutId, 10)) {
                                    return workouts[i];
                                }
                            }
                            return null;
                        }
                    };
            },

            GetDateTimeOptions: function(modeType) {
                var options = {
                    date: new Date(),
                    mode: modeType, // or 'time'
                    minDate: new Date() - 10000,
                    //allowOldDates: true,
                    allowFutureDates: true,
                    doneButtonLabel: 'DONE',
                    doneButtonColor: '#F2F3F4',
                    cancelButtonLabel: 'CANCEL',
                    cancelButtonColor: '#000000'
                };
                return options;


            },

            GetWorkoutLocations: function() {
                var locations = [
                    { id: 0, name: 'In My Home' },
                    { id: 1, name: 'In My Apartment Gym' },
                    { id: 2, name: 'At My Gym (Membership Required)' },
                    { id: 3, name: 'Park or Outside' },
                    { id: 4, name: 'Trainer\'s Choice' }
                ];
                return {
                    all: function() {
                        return locations;
                    },
                    get: function(locationId) {
                        for (var i = 0; i < locations.length; i++) {
                            if (locations[i].id === parseInt(locationId, 10)) {
                                return locations[i];
                            }
                        }
                        return null;
                    }
                };
            },



        }

        return _service ;

    });
