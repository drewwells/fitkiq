var svc = angular.module('fitkiq.workout', []);

svc.service('workoutService', function(
  $q,
  $http
) {

  return {
    all: all
  };


  function all() {
    return $q(function(resolve, reject) {
      $http.get('/v1/services/').then(function(resp) {
        if (!resp.data) {
          reject("error contacting server");
        }
        resolve(resp.data);
      }, function(err) {
        reject(err.data);
      })
    })
  }
});
