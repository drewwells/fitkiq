var loc = angular.module('fitkiq.location', ['ngMap']);

loc.config(function($stateProvider) {
  $stateProvider
    .state('workout-location', {
      url: '/schedule/:workoutType/:workout/:from/:to',
      controller: 'LocationCtrl as loc',
      templateUrl: 'templates/schedule-map.html',
    })
});

loc.controller('LocationCtrl', function($state, $stateParams) {
  var loc = this;
  loc.address = "123 E. Main St. Austin, TX";

  loc.submitlocation = function() {
    $state.go('picktrainer', {
      workoutType: $stateParams.workoutType,
      workout: $stateParams.workout,
      from: $stateParams.from,
      to: $stateParams.to,
      loc: loc.address
    })
  }
});
