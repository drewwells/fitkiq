tmod.config(function($stateProvider) {
  $stateProvider.state('picktrainer', {
    url: '/schedule/:workoutType/:workout/:from/:to/:loc',
    controller: 'TrainerCtrl as scope',
    templateUrl: 'js/trainers/pick.html'
  })
})

tmod.controller('TrainerCtrl', function($stateParams, trainerService) {
  var scope = this;

  trainerService.all().then(function(ts) {
    scope.trainers = ts;
  }, function(err) {
    console.log('error retrieving trainers', err);
  })

});
