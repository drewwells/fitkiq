var tmod = angular.module('fitkiq.trainer', []);

tmod.service('trainerService', function(
  $q,
  $http,
  $filter
) {

  var _slots = {};

  return {
    all: all,
    slots: function() {
      return _slots;
    },
    fetchSlots: fetch
  };

  function fetch(resourceID, from, to) {
    var sfrom = $filter('date')(from,'yyyy-MM-dd');
    var sto = $filter('date')(to,'yyyy-MM-dd');

    var path = "/v1/services/" + resourceID + "/slots/?from=" +
          sfrom + "&to=" + sto;

    return $q(function(resolve, reject) {
      $http.get(path).then(function(resp) {
        if (!resp.data) {
          reject("error contacting server");
        }
        resolve(resp.data);
        _slots = resp.data;
      }, function(err) {
        reject(err.data);
      })
    });
  }

  function all() {
    return $q(function(resolve, reject) {
      $http.get('/v1/trainers/').then(function(resp) {
        if (!resp.data) {
          reject("error contacting server");
          return
        }
        resolve(resp.data);
      }, function(err) {
        reject(err.data);
      })
    })
  }
})
