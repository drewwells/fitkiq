// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var app = angular.module('fitkiq', [
  'ngCookies',
  'ionic',
  'fitkiq.controllers',
  'fitkiq.services',
  'fitkiq.location',
  'fitkiq.schedule',
  'fitkiq.auth',
  'ngCordova',
  'ngMap'
])
.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }

  });
})
.config(function($ionicConfigProvider, $stateProvider, $urlRouterProvider) {

  $ionicConfigProvider.tabs.position('bottom'); // other values: top

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })
  // Each tab has its own nav history stack:
  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })
      .state('tab.friends', {
        url: '/friends',
        views: {
          'tab-friends': {
            templateUrl: 'templates/tab-friends.html',
            controller: 'FriendsCtrl'
          }
        }
      })
  .state('tab.messages', {
      url: '/messages',
      views: {
        'tab-messages': {
          templateUrl: 'templates/tab-messages.html',
          controller: 'MessagesCtrl'
        }
      }
    })
    .state('tab.message-detail', {
      url: '/messages/:messageId',
      views: {
        'tab-messages': {
          templateUrl: 'templates/message-detail.html',
          controller: 'MessageDetailCtrl'
        }
      }
    })

      .state('tab.schedule', {
        url: '/schedule',
        views: {
          'tab-schedule': {
            templateUrl: 'templates/tab-schedule.html',
            controller: 'ScheduleCtrl as vm'
          }
        }
      })
      .state('tab.workout-schedule', {
        url: '/schedule/:workoutType/:workout',
        views: {
          'tab-schedule': {
            templateUrl: 'templates/workout-schedule.html',
            controller: 'ScheduleWorkoutCtrl as vm'
          }
        }
      })

      // .state('tab.workout-location', {
      //   url: '/schedule/:workoutType/:workout/:from/:to',
      //   views: {
      //     'tab-schedule': {
      //       templateUrl: 'templates/schedule-map.html',
      //       controller: 'LocationCtrl as loc'
      //     }
      //   }
      // });

      // .state('tab.schedule-map', {
      //   url: '/schedule/:workoutType/:workout',
      //   views: {
      //     'tab-schedule': {
      //       templateUrl: 'templates/schedule-map.html',
      //       controller: 'ScheduleMapCtrl as vm'
      //     }
      //   }
      // });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/schedule');

});

app.run(['$rootScope', '$location', 'authService', function($rootScope, $location, auth) {
  $rootScope.$on('$locationChangeStart', function(ev) {
    if (!/^\/login/.test($location.path())) {
      if (!auth.token()) {
        // ev.preventDefault();
        // $location.path('/login');
      }
    }
  });
}]);

angular.module('fitkiq').factory('authInterceptorService', [
  '$q',
  '$cookies',
  function(
  $q,
  $cookies
){

  // We need some build pragma to make decisions between these
  var localRoot = "http://localhost:8080";
  var testRoot = "http://test.appspot.com";
  var prodRoot = "http://api.appspot.com";
  var root = window.location.origin;

  return {

    request: function(config) {
      var path = config.url;

      // Some sloppy detecting for XHR calls to API_key
      if (path.indexOf("/v1/")  == -1 &&
          path.indexOf("/admin/") == -1 ) {
        return config
      }

      // Prefix requested path with API root URI
      config.url = root + path;

      // Special exception for the auth path, which requires no auth.
      if (path == '/v1/login') {
        return config;
      }

      var token = $cookies.get('token', {path:'/'});
      config.headers['Authorization'] = 'Bearer '+ token;

      return config;
    },

    responseError: function(response) {
      if (response.status == 401) {
        $cookies.remove('token', {path:'/'});
        $rootScope.loggedIn = false;
      }
      return $q.reject(response);
    }
  };

}]);

angular.module("fitkiq").config(function($httpProvider) {
  $httpProvider.interceptors.push('authInterceptorService');
});
