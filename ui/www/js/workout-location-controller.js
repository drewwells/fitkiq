angular.module('fitkiq')
  .controller('WorkoutLocationCtrl', function(
    $scope,
    DataService,
    $state,
    $stateParams,
    trainerService
  ) {

    var vm = this;
    vm.selectedLocation = {};
    vm.locations = DataService.GetWorkoutLocations().all();


    // Find services, populate and go to next page
    trainerService.fetchSlots(
      $stateParams.workout,
      $stateParams.from,
      $stateParams.to
    ).then(function(slots) {
      vm.slots = slots;
      console.log(slots);
    }, function(err) {
      console.log('error retrieving slots', err);
    })

    vm.ChooseLocation = function(obj) {
      var dataValue = obj.target.attributes.data.value;
      obj.target.classList.toggle('active');

      [].map.call(document.querySelectorAll('.location'), function(el) {
        if (obj.target.attributes.data.value !== el.attributes.data.value) {
          el.classList.remove('active');
        }
      });

      if (obj.target.classList.contains('active')) {
        vm.selectedLocation.Value = dataValue;

      } else {
        vm.selectedLocation.Value = "";
      }

    };

    vm.SubmitLocation = function() {
      $state.go('tab.schedule-map', {  });
    };

  });
