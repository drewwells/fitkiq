angular.module("fitkiq")
  .service("authService", function($cookies, $http, $q) {

    var _token = $cookies.get('token');
    var _user = {};
    var callbacks = [];

    if (_token) {
      profile();
    }

    return {
      token: token,
      auth: auth,
      profile: profile,
      user: user,
      logout: logout,
      loginFB: loginFB,
      register: register
    };

    // register callbacks
    function register(fn) {
      callbacks.push(fn);
    }

    function loginFB(authResponse) {
      return $q(function(resolve, reject) {
        $http.post("/v1/fblogin", authResponse).then(function(resp) {
          if (!resp.data) {
            reject("fb login data missing");
            return;
          }
          if (!resp.data.user) {
            reject("fb login user missing");
            return;
          }

          save(resp.data.user);
          resolve(resp.data.user);
        }, function(err) {
          reject(err.data);
        })
      });
    }

    function logout() {
      $cookies.remove('token', {path:'/'});
      _user = {};
      _token = "";
    }

    function user() {
      return _user;
    }

    function token() {
      return _token;
    }

    // useful for when token is present and need to refresh
    // user's profile
    function profile() {
      if (!_token.length) {
        console.log("fail!");
        return;
      }

      $http.get("/v1/profile").then(function(response) {
        if (!response.data) {
          console.log("something went wrong");
          return
        }
        if (!response.data.user) {
          console.log("user not found");
          return;
        }
        save(response.data.user);
      });
    }

    // Notify callbacks in separate loop of updates
    function notify(user) {
      setTimeout(function() {
        for (var i = 0; i < callbacks.length; i++) {
          callbacks[i](user);
        }
      }, 4);
    }

    function save(user) {
      _user = user;
      _token = user.token;
      notify(user);
      $cookies.put("token", user.token, {path:"/"});
    }

    // exchange email/pass for token to authenticate against API
    function auth(email, pass) {
      return $http.post("/v1/login", {email: email, password: pass})
        .then(function(response) {
          if (!response.data) {
            return;
          }

          if (!response.data.user) {
            return;
          }

          save(response.data.user);
          return _user;
        });
    }
  });
