var auth = angular.module('fitkiq.auth', [])

auth.run([
  '$rootScope',
  '$window',
  'authService',
  function($rootScope, $window, sAuth) {

    $rootScope.user = {};

    function getUserInfo() {
      // scope much?
      FB.api('/me', function(res) {
        console.log('whoami?', res);
      })
    }

    (function(d){
      // load the Facebook javascript SDK
      var js,
          id = 'facebook-jssdk',
          ref = d.getElementsByTagName('script')[0];

      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement('script');
      js.id = id;
      js.async = true;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      ref.parentNode.insertBefore(js, ref);
    }(document));

  }]);

auth.config(function($stateProvider) {
  $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: 'js/auth/login.html',
      controller: 'LoginCtrl'
    })

});
