auth.controller('ProfileCtrl', [
  'authService',
  '$scope',
  function(
    authService,
    $scope
  ) {
    var profile = this;

    authService.register(function(user) {
      profile.user = user;
      profile.showuser = true;
    });
  }
]
)

auth.controller(
  'LoginCtrl', [
    'authService',
    '$window',
  function(
    authService,
    $window
  ) {

    $window.fbAsyncInit = function() {
      // Executed when the SDK is loaded

      FB.init({
        appId: '1626862994260188',
        status: true,
        cookie: true,
        xfbml: true,
        version: 'v2.2'
      });

      FB.Event.subscribe('auth.statusChange', function(res) {
        if (authService.token()) {
          return
        }
        if (res.status === 'connected') {
          loginFB(res.authResponse);
        }
      });

      FB.Event.subscribe('auth.authResponseChange', function(res) {
        if (authService.token()) {
          return
        }
        if (res.status === 'connected') {
          loginFB(res.authResponse);
        }
      })

    };

    var _authed = false;

    authService.register(function(user) {
      if (user) {
        _authed = true;
      }
    })

    return {
      dologin: dologin,
      loginFB: loginFB,
      authed: _authed,
      logout: function() {
        authService.logout();
        $window.location.reload();
      }
    };

    function loginFB(authResponse) {
      var self = this;
      this.$error = {};
      return authService.loginFB(authResponse).then(function(user) {
        console.log('fb authed', user);
      }, function(err) {
        if (err === "email is registered") {
          self.$error["email is registered"] = true;
        }
      });
    }

    function dologin(data) {
      var self = this;
      authService.logout();
      this.$error = {};
      authService.auth(this.email, this.password).then(function(profile) {
        console.log('accept', profile);
      }, function(reject) {
        console.log('reject', reject);
        if (!reject.data) {
          self.$error["server"] = true;
        } else if (reject.data.error == "user not found") {
          self.$error["notfound"] = true;
        } else {
          console.log(reject.data.error)
          self.$error[reject.data.error] = true
        }
      })
    };
  }]);
