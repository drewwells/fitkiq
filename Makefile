NAME=api
COMMIT=$(shell GCOMMIT=`git rev-parse --short HEAD`; if [ -n "$$(git status --porcelain)" ]; then echo "$$GCOMMIT-dirty"; else echo $$GCOMMIT; fi)
VERSION=$(shell cat VERSION)
VERSION_PKG=bitbucket.org/fitkiq/api/versionpkg
BRANCH=$(shell git symbolic-ref -q HEAD 2>/dev/null|awk '{c=split($$0,elem,"/"); if (c == 3) {printf("%s", elem[3])} else {printf("%s/%s", elem[3], elem[4])} }' || echo unknown)


BUILDCOMMIT=-X $(VERSION_PKG).Commit $(COMMIT)
BUILDVERSION=-X $(VERSION_PKG).Version $(VERSION)
BUILDBRANCH=-X $(VERSION_PKG).Branch $(BRANCH)
BUILD=$(BUILDCOMMIT) $(BUILDVERSION) $(BUILDBRANCH)
APPYAML=gae/app.yaml

$(NAME): build
	build/api
	# cd ui; ionic serve


.PHONY: build
build: account
	mkdir -p build
	cd api; go build  -o ../build/api -ldflags "$(BUILD)"

.PHONY: gae
serve: account
	goapp serve $(APPYAML)

gae/account.json:
	@ test ! -f gae/account.json && $(error account.json missing, this is required to authenticate with makeplans API)

account: gae/account.json

copy-ui:
	cp -R ui/www/ gae/public/ui

deploy: account swagger copy-ui
	goapp deploy $(APPYAML)

$(LASTGOPATH/bin/swagger):
	@go get github.com/go-swagger/go-swagger/cmd/swagger

swagger: $(LASTGOPATH/bin/swagger)
	swagger generate spec | jq '.' > gae/swagger.json
