// +build !appengine

package api

import "net/http"

func init() {
	getHTTPCli = func(r *http.Request) *http.Client {
		return &http.Client{}
	}

	IsDev = func() bool {
		return true
	}
}
