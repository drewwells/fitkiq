package api

import (
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/drewwells/fitkiq/types"
	gcontext "github.com/gorilla/context"
	"golang.org/x/net/context"
)

const bearerPrefix = "Bearer "

type ctxkey int

const (
	UserKey ctxkey = iota
)

var errUserNotFound = errors.New("user not found")
var errUserNotTrainer = errors.New("user not trainer")
var errUserNotAdmin = errors.New("user not admin")

func UserFromRequest(r *http.Request) (types.User, error) {
	v := gcontext.Get(r, UserKey)
	if user, ok := v.(types.User); ok {
		return user, nil
	}
	return types.User{}, errUserNotFound
}

func mwadmin(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	if isErrBadRequest(w, r, isadmin(r)) {
		return
	}

	if next != nil {
		next(w, r)
	}
}

func mwtrainer(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	if isErrBadRequest(w, r, istrainer(r)) {
		return
	}

	if next != nil {
		next(w, r)
	}
}

func isadmin(r *http.Request) error {
	user := gcontext.Get(r, UserKey).(types.User)

	if user.Role != types.AdminRole {
		return errUserNotAdmin
	}

	return nil
}

func istrainer(r *http.Request) error {
	user := gcontext.Get(r, UserKey).(types.User)

	if user.Role != types.TrainerRole && user.Role != types.AdminRole {
		return errUserNotTrainer
	}

	return nil
}

func (s *Server) token(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {

	var bearer string
	authHeaders := r.Header["Authorization"]
	for _, str := range authHeaders {
		if strings.HasPrefix(str, bearerPrefix) {
			bearer = strings.TrimPrefix(str, bearerPrefix)
		}
	}

	if len(bearer) == 0 {
		httpError(w, r, "bearer not found, use header Authorization: Bearer {token}", http.StatusBadRequest)
		return
	}

	db := FromRequest(r)
	users, err := db.GetUsers(context.TODO(), types.User{Token: bearer})
	if isErrInternal(w, r, err) {
		return
	}
	if len(users) != 1 {
		httpError(w, r, "authentication failed, bearer token is invalid", http.StatusInternalServerError)
		return
	}

	gcontext.Set(r, UserKey, users[0])
	if next != nil {
		next(w, r)
	}
}

// Login swagger:route POST /v1/login auth Login
//
// Retrieve token with user:pass
//
// Consumes:
// application/json
//
// Schemes: http, https
//
// Produces:
// application/json
//
// Responses:
// 200: UserResponse
func (s *Server) login(w http.ResponseWriter, r *http.Request) {
	var user types.User
	err := readBody(r.Body, &user)
	if isErrBadRequest(w, r, err) {
		return
	}

	if len(user.Email) == 0 {
		httpError(w, r, "email is required", http.StatusBadRequest)
		return
	}

	if len(user.Password) == 0 {
		httpError(w, r, "password is required", http.StatusBadRequest)
		return
	}

	db := FromRequest(r)
	users, err := db.GetUsers(context.TODO(), user)
	if isErrInternal(w, r, err) {
		return
	}

	if len(users) != 1 {
		fmt.Printf("% #v\n", users)
		httpError(w, r, "user not found", http.StatusInternalServerError)
		return
	}
	uu := users[0]
	uu.Password = ""
	resp := types.UserResponse{User: uu}

	marshal(w, r, resp)
}
