package facade

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"golang.org/x/net/context"

	"github.com/drewwells/fitkiq/schedule"
	"github.com/drewwells/fitkiq/store"
	"github.com/drewwells/fitkiq/types"
)

type Auth struct {
	db  *store.DB
	sch *schedule.Schedule
}

func NewAuth(sch *schedule.Schedule, db *store.DB) (*Auth, error) {
	return &Auth{
		sch: sch,
		db:  db,
	}, nil
}

var (
	graphAPI   = "https://graph.facebook.com/v2.5/%s?fields=email,first_name,last_name,friends,id&access_token=%s"
	graphDebug = "https://graph.facebook.com/debug_token?input_token=%s&access_token=%s"
	//&amp;access_token={app-token-or-admin-token}"
	graphOauth    = "https://graph.facebook.com/oauth/access_token?client_id=%s&client_secret=%s&grant_type=client_credentials"
	profileLayout = "https://graph.facebook.com/v2.5/%s/picture?type=large"
)

var appID = "1626862994260188"

// FIXME: move this to an environment variable
var appSecret = "0e9afd33b7474b533796572e6c5cdfee"
var appToken string

// GET /oauth/access_token?
// client_id={app-id}
// &amp;client_secret={app-secret}
// &amp;grant_type=client_credentials
func getAppToken(cli *http.Client) string {
	resp, err := cli.Get(fmt.Sprintf(graphOauth, appID, appSecret))
	if err != nil {
		log.Printf("failed to retrieve oauth token: %s", err)
		return ""
	}
	defer resp.Body.Close()
	fmt.Fscanf(resp.Body, "access_token=%s", &appToken)
	return appToken
}

type wrapDebugToken struct {
	Data types.DebugToken `json:"data"`
}

// Facebook validates a facebook token and optionally creates a user.
// A token is returned for use in login
func (a *Auth) Facebook(ctx context.Context, auth types.FBAuth) (user types.User, err error) {
	cli := types.ClientFromCtx(ctx)
	apptoken := getAppToken(cli)

	// Round trip to validate the token
	path := fmt.Sprintf(graphDebug, auth.AccessToken, apptoken)
	req, err := http.NewRequest("GET", path, nil)
	if err != nil {
		return
	}
	fbresp, err := cli.Do(req)
	if err != nil {
		return
	}
	var wrap wrapDebugToken

	err = readBody(fbresp.Body, &wrap)
	if err != nil {
		return
	}

	debug := wrap.Data

	if !debug.IsValid || debug.AppID != appID {
		err = errors.New("token is invalid")
		return
	}

	// Round trip to retrieve user info
	path = fmt.Sprintf(graphAPI, auth.UserID, auth.AccessToken)
	req, err = http.NewRequest("GET", path, nil)
	if err != nil {
		return
	}
	resp, err := cli.Do(req)
	if err != nil {
		err = fmt.Errorf("error retrieving user info: %s", err)
		return
	}

	var profile types.GraphProfile
	err = readBody(resp.Body, &profile)
	if err != nil {
		return
	}

	user, err = a.db.GetUser(context.TODO(), types.User{
		Email: profile.Email,
	})

	if err != nil {
		// Create a user when it can't be found
		new := types.User{}
		new.Email = profile.Email
		new.Role = types.MemberRole
		new.FirstName = profile.FirstName
		new.LastName = profile.LastName
		new.Picture = fmt.Sprintf(profileLayout, profile.ID)
		user, err = a.db.AddUser(context.TODO(), new)
		if err != nil {
			return
		}
	}

	return
}

func readBody(r io.ReadCloser, v interface{}) error {
	if r == nil {
		return errors.New("request body is empty")
	}
	defer r.Close()
	bs, err := ioutil.ReadAll(r)
	if err != nil {
		return err
	}
	return json.Unmarshal(bs, v)
}
