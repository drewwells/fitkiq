package facade

import (
	"errors"
	"fmt"

	"github.com/drewwells/fitkiq/schedule"
	"github.com/drewwells/fitkiq/store"
	"github.com/drewwells/makeplans"
)

type Booking struct {
	db  *store.DB
	sch *schedule.Schedule
}

func NewBooking(sch *schedule.Schedule, db *store.DB) (*Booking, error) {
	b := &Booking{
		sch: sch,
		db:  db,
	}
	return b, nil
}

func (b *Booking) Get(bookingID int, personID int) (makeplans.Booking, error) {
	// personID == 0 is admin
	if personID == 0 {
		return b.sch.Booking(bookingID)
	}

	books, err := b.List(makeplans.BookingParams{PersonID: personID})
	if err != nil {
		return makeplans.Booking{}, err
	}

	for _, book := range books {
		if book.PersonID == personID && book.ID == bookingID {

			return book, nil
		}
	}
	return makeplans.Booking{}, errors.New("booking not found")
}

func (b *Booking) Update(book makeplans.Booking) (makeplans.Booking, error) {
	book, err := b.Get(book.ID, book.PersonID)
	if err != nil {
		return makeplans.Booking{}, fmt.Errorf("failed to update booking: %s", err)
	}

	return b.sch.BookingUpdate(book)
}

func (b *Booking) Delete(bookingID int, personID int) error {
	// personID == 0 is admin
	if personID == 0 {
		_, err := b.sch.BookingDelete(bookingID)
		return err
	}

	books, err := b.List(makeplans.BookingParams{PersonID: personID})
	if err != nil {
		return err
	}

	for _, book := range books {
		if book.PersonID == personID && book.ID == bookingID {
			_, err := b.sch.BookingDelete(bookingID)
			return err
		}
	}
	return errors.New("booking not found")
}

func (b *Booking) List(params makeplans.BookingParams) ([]makeplans.Booking, error) {

	return b.sch.Bookings(params)
}
