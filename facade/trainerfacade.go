package facade

import (
	"errors"

	"github.com/drewwells/fitkiq/schedule"
	"github.com/drewwells/fitkiq/store"
	"github.com/drewwells/fitkiq/types"
	"github.com/drewwells/makeplans"
	"golang.org/x/net/context"
)

type Trainer struct {
	id      string
	db      *store.DB
	sch     *schedule.Schedule
	trainer types.User
}

func NewTrainer(sch *schedule.Schedule, db *store.DB, id string) (*Trainer, error) {

	if len(id) == 0 {
		return nil, errors.New("invalid trainer ID")
	}

	t := &Trainer{
		sch: sch,
		db:  db,
		id:  id,
	}
	trainer, err := t.load(db, sch, id)
	if err != nil {
		return nil, err
	}
	t.trainer = trainer
	return t, nil
}

func (t *Trainer) Get() types.User {
	return t.trainer
}

func (s *Trainer) Services() ([]makeplans.Service, error) {
	// FIXME: Ugh this is messy, we should populate this somewhere locally
	prvs, err := s.sch.Providers()
	if err != nil {
		return nil, err
	}
	// For now, pulling all services would be faster than looking at
	// each one individually
	svcs, err := s.sch.Services()
	if err != nil {
		return nil, err
	}

	var pprvs []makeplans.Provider
	for _, p := range prvs {
		if p.ResourceID != s.trainer.ResourceID {
			continue
		}
		pprvs = append(pprvs, p)
	}

	ssvcs := make([]makeplans.Service, 0, len(pprvs))
	for _, svc := range svcs {
		for _, p := range pprvs {
			if svc.ID != p.ServiceID {
				continue
			}
			ssvcs = append(ssvcs, svc)
		}
	}

	return ssvcs, nil
}

func (p *Trainer) Providers() ([]makeplans.Provider, error) {
	provs, err := p.sch.Providers()
	if err != nil {
		return nil, err
	}
	filtered := make([]makeplans.Provider, 0)
	// FIXME: this should pass along resource filter to makeplans api
	for _, prov := range provs {
		if prov.ResourceID == p.trainer.ResourceID {
			filtered = append(filtered, prov)
		}
	}
	return filtered, nil
}

func (p *Trainer) MakeProvider(svcID int) (makeplans.Provider, error) {
	prov := makeplans.Provider{
		ResourceID: p.trainer.ResourceID,
		ServiceID:  svcID,
	}

	return p.sch.MakeProvider(prov)
}

func (p *Trainer) DeleteTrainerService(sid int) (int, error) {
	prvs, err := p.sch.Providers()
	if err != nil {
		return 0, err
	}

	var prvID int
	for _, prv := range prvs {
		if prv.ResourceID == p.trainer.ResourceID &&
			prv.ServiceID == sid {
			prvID = prv.ID
		}
	}

	_, err = p.sch.ProviderDelete(prvID)
	return prvID, err
}

func (t *Trainer) load(db *store.DB, sch *schedule.Schedule, id string) (types.User, error) {
	var ret types.User
	users, err := db.GetUsers(context.TODO(), types.User{
		ID: id,
	})
	if len(users) != 1 {
		return ret, errors.New("trainer user not found")
	}

	rsc, err := sch.Resource(users[0].ResourceID)
	if err != nil {
		return ret, err
	}

	if rsc.ID == 0 {
		return ret, errors.New("Resource not found in makeplans API")
	}

	return users[0], err
}

func Trainers(db *store.DB, sch *schedule.Schedule, public bool) ([]types.User, error) {
	ctx := context.TODO()
	users, err := db.GetUsers(ctx, types.User{
		Role: types.TrainerRole,
	})
	// Retrieve resources (makeplans representation of a trainer)
	ress, err := sch.Resources()
	if err != nil {
		return nil, err
	}
	idxRess := make(map[int]makeplans.Resource, len(ress))
	for _, res := range ress {
		idxRess[res.ID] = res
	}
	ts := make([]types.User, 0, len(users))
	for _, u := range users {
		r, ok := idxRess[u.ResourceID]
		if !ok {
			continue
		}
		u.Resource = &r
		u.Password = ""
		u.Token = ""
		if public {
			// Also remove other private information when public
			u.Email = ""
			u.Resource = nil
		}
		ts = append(ts, u)
	}
	return ts, nil
}
