package api

import "text/template"

type routeTemplate struct {
	Title  string
	Prefix string
	Links  []string
}

const printTpl = `{{$out := . }}
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>{{.Title}}</title>
	</head>
	<body>
{{range .Links}}<a href="{{$out.Prefix}}{{.}}">{{$out.Prefix}}{{.}}</a><br/>{{end}}
	</body>
</html>`

var tpls *template.Template

func init() {
	tpls = template.Must(template.New("printroutes").Parse(printTpl))
}
