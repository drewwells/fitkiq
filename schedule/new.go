package schedule

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/drewwells/makeplans"
)

type Schedule struct {
	client  *makeplans.Client
	HTTPCli *http.Client
}

type account struct {
	Name  string
	Token string
}

func New(file string) (*Schedule, error) {
	bs, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	var ac account
	json.Unmarshal(bs, &ac)
	// Should specify these somewhere
	return &Schedule{
		client: makeplans.New(ac.Name, ac.Token),
	}, nil
}

func (s *Schedule) setHTTPCli() {
	if s.HTTPCli != nil {
		s.client.Client = s.HTTPCli
	}
}

func (s *Schedule) MakeBooking(b makeplans.Booking) (makeplans.Booking, error) {
	s.setHTTPCli()
	return s.client.MakeBooking(b)
}

func (s *Schedule) BookingUpdate(b makeplans.Booking) (makeplans.Booking, error) {
	s.setHTTPCli()
	return s.client.BookingUpdate(b)
}

func (s *Schedule) Booking(bookingID int) (makeplans.Booking, error) {
	s.setHTTPCli()
	return s.client.Booking(bookingID)
}

func (s *Schedule) Bookings(p makeplans.BookingParams) ([]makeplans.Booking, error) {
	s.setHTTPCli()
	return s.client.Bookings(p)
}

func (s *Schedule) BookingDelete(id int) (makeplans.Booking, error) {
	s.setHTTPCli()
	return s.client.BookingDelete(id)
}

func (s *Schedule) Services() ([]makeplans.Service, error) {
	s.setHTTPCli()
	return s.client.Services()
}

func (s *Schedule) MakeService(svc makeplans.Service) (makeplans.Service, error) {
	s.setHTTPCli()
	return s.client.ServiceCreate(svc)
}

func (s *Schedule) ServiceUpdate(svc makeplans.Service) (makeplans.Service, error) {
	s.setHTTPCli()
	return s.client.ServiceSave(svc)
}

func (s *Schedule) ServiceDelete(id int) (makeplans.Service, error) {
	s.setHTTPCli()
	return s.client.ServiceDelete(id)
}

func (s *Schedule) ServiceSlot(serviceID int, params makeplans.SlotParams) ([]makeplans.Slot, error) {
	s.setHTTPCli()
	return s.client.ServiceSlot(serviceID, params)
}

func (s *Schedule) MakePerson(p makeplans.Person) (makeplans.Person, error) {
	s.setHTTPCli()
	return s.client.MakePerson(p)
}

func (s *Schedule) Resources() ([]makeplans.Resource, error) {
	s.setHTTPCli()
	return s.client.Resources()
}

func (s *Schedule) Resource(id int) (makeplans.Resource, error) {
	s.setHTTPCli()
	return s.client.Resource(id)
}

func (s *Schedule) ResourceUpdate(r makeplans.Resource) (makeplans.Resource, error) {
	s.setHTTPCli()
	return s.client.ResourceUpdate(r)
}

func (s *Schedule) MakeResource(r makeplans.Resource) (makeplans.Resource, error) {
	s.setHTTPCli()
	return s.client.MakeResource(r)
}

func (s *Schedule) ResourceDelete(id int) (makeplans.Resource, error) {
	s.setHTTPCli()
	return s.client.ResourceDelete(id)
}

func (s *Schedule) Providers() ([]makeplans.Provider, error) {
	s.setHTTPCli()
	return s.client.Providers()
}

func (s *Schedule) MakeProvider(in makeplans.Provider) (makeplans.Provider, error) {
	s.setHTTPCli()
	return s.client.MakeProvider(in)
}

func (s *Schedule) ProviderUpdate(in makeplans.Provider) (makeplans.Provider, error) {
	s.setHTTPCli()
	return s.client.ProviderUpdate(in)
}

func (s *Schedule) ProviderDelete(id int) (makeplans.Provider, error) {
	s.setHTTPCli()
	return s.client.ProviderDelete(id)
}
