// +build appengine

package fitkiq

import (
	"log"

	"github.com/drewwells/fitkiq"
	"github.com/drewwells/makeplans"
)

// If prod?

func init() {
	makeplans.DefaultURL = "http://%s.test.makeplans.net/api/v1"
	_, err := api.New("account.json")
	if err != nil {
		log.Println("cant start the server", err)
		return
	}
}
