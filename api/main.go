// +build !appengine

package main

import (
	"log"
	"net/http"

	"github.com/drewwells/fitkiq"
)

func main() {
	_, err := api.New("gae/account.json")
	if err != nil {
		log.Fatal(err)
	}
	host := ":8080"

	log.Println("Server started at", host)
	log.Fatal(http.ListenAndServe(host, nil))
}
