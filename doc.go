// Package classification API.
//
// the purpose of this application is to provide an application
// that is using plain go code to define an API
//
//
//     Schemes: https
//     Host: skillful-elf-105206.appspot.com
//     Version: 0.0.1
//     Contact: Drew Wells<drew@fitkiq.com> http://fitkiq.com
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//
// swagger:meta
package api
