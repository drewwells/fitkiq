package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/drewwells/fitkiq/facade"
	"github.com/drewwells/fitkiq/types"
	"github.com/drewwells/makeplans"
	"github.com/gorilla/mux"
)

// Profile swagger:route GET /v1/profile user Profile
// Retrieves profile information about the user
//
// Retrieves profile information about the user
//
// Consumes:
// application/json
//
// Schemes: http, https
//
// Produces:
// application/json
//
// Responses:
//       200: UserResponse
func (s *Server) userprofile(w http.ResponseWriter, r *http.Request) {

	user, err := UserFromRequest(r)

	if isErrBadRequest(w, r, err) {
		return
	}
	user.Password = ""
	marshal(w, r, types.UserResponse{User: user})
}

// UserBooking swagger:route GET /v1/bookings/{id} user UserBooking
//
// Bookings lists an appointments associated with this user.
//
// Consumes:
// application/json
//
// Schemes: http, https
//
// Produces:
// application/json
//
// Responses:
// 200: BookingResponse
func (s *Server) userbooking(w http.ResponseWriter, r *http.Request) {
	user, err := UserFromRequest(r)
	if isErrBadRequest(w, r, err) {
		return
	}
	db := FromRequest(r)
	fac, err := facade.NewBooking(s.sch, db)
	if isErrInternal(w, r, err) {
		return
	}

	sid := mux.Vars(r)["id"]
	id, err := strconv.Atoi(sid)
	if isErrBadRequest(w, r, err) {
		return
	}

	book, err := fac.Get(id, user.PersonID)

	if isErrInternal(w, r, err) {
		return
	}

	marshal(w, r, types.BookingResponse{
		Booking: book,
	})
}

// BookingUpdate swagger:route DELETE /v1/bookings/{id} user BookingUpdate
//
// Removes a booking if the user is associated with it
//
// Consumes:
// application/json
//
// Schemes: http, https
//
// Produces:
// application/json
//
// Responses:
// 200: Response
func (s *Server) userbookingdelete(w http.ResponseWriter, r *http.Request) {
	sid := mux.Vars(r)["id"]
	id, err := strconv.Atoi(sid)

	if isErrBadRequest(w, r, err) {
		return
	}

	user, err := UserFromRequest(r)
	if isErrBadRequest(w, r, err) {
		return
	}
	db := FromRequest(r)
	fac, err := facade.NewBooking(s.sch, db)
	if isErrInternal(w, r, err) {
		return
	}

	err = fac.Delete(id, user.PersonID)
	if err != nil {
		httpError(w, r, err.Error(), 500)
		return
	}
	marshal(w, r, Response{
		Success: fmt.Sprintf("deleted booking: %d", id),
	})
}

// BookingUpdate swagger:route DELETE /v1/bookings/{id} user BookingUpdate
//
// Removes a booking if the user is associated with it
//
// Consumes:
// application/json
//
// Schemes: http, https
//
// Produces:
// application/json
//
// Responses:
// 200: Response
func (s *Server) userbookingupdate(w http.ResponseWriter, r *http.Request) {
	sid := mux.Vars(r)["id"]
	id, err := strconv.Atoi(sid)

	if isErrBadRequest(w, r, err) {
		return
	}

	user, err := UserFromRequest(r)
	if isErrBadRequest(w, r, err) {
		return
	}

	db := FromRequest(r)
	fac, err := facade.NewBooking(s.sch, db)
	if isErrInternal(w, r, err) {
		return
	}

	bs, err := ioutil.ReadAll(r.Body)
	if isErrBadRequest(w, r, err) {
		return
	}
	defer r.Body.Close()
	var book makeplans.Booking
	err = json.Unmarshal(bs, &book)
	if isErrInternal(w, r, err) {
		return
	}

	if user.PersonID != book.PersonID {
		httpError(w, r, "person id does not match one found in booking", http.StatusBadRequest)
	}

	if id != book.ID {
		httpError(w, r, "booking id in URL does not match one in body", http.StatusBadRequest)
	}

	ret, err := fac.Update(book)
	if err != nil {
		httpError(w, r, err.Error(), 500)
		return
	}
	marshal(w, r, ret)
}

// UserBookings swagger:route GET /v1/bookings/ user UserBookings
//
// Bookings lists all the appointments that have been made for the current
// user.
//
// Consumes:
// application/json
//
// Schemes: http, https
//
// Produces:
// application/json
//
// Responses:
// 200: BookingResponses
func (s *Server) userbookings(w http.ResponseWriter, r *http.Request) {
	user, err := UserFromRequest(r)
	if isErrBadRequest(w, r, err) {
		return
	}

	books, err := s.sch.Bookings(makeplans.BookingParams{
		PersonID: user.PersonID,
	})
	if isErrInternal(w, r, err) {
		return
	}

	filtered := []makeplans.Booking{}
	for _, book := range books {
		if book.PersonID != user.PersonID {
			continue
		}
		filtered = append(filtered, book)
	}

	marshal(w, r, types.BookingResponses{
		Bookings: filtered,
	})
}

// MakeBooking swagger:route POST /v1/bookings/ user MakeBooking
//
// Create a new booking. It must match an available slot on a resource
//
//
// Consumes:
// application/json
//
// Schemes: http, https
//
// Produces:
// application/json
//
// Responses:
// 200: BookingResponse
func (s *Server) makeBooking(w http.ResponseWriter, r *http.Request) {
	user, err := UserFromRequest(r)
	if isErrBadRequest(w, r, err) {
		return
	}

	var b makeplans.Booking
	if isErrBadRequest(w, r, readBody(r.Body, &b)) {
		return
	}

	b.PersonID = user.PersonID

	book, err := s.sch.MakeBooking(b)
	if err != nil {
		httpError(w, r, err.Error(), 500)
		return
	}

	marshal(w, r, types.BookingResponse{
		Booking: book,
	})
}
