package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"golang.org/x/net/context"

	"github.com/drewwells/fitkiq/facade"
	"github.com/drewwells/fitkiq/types"
	"github.com/drewwells/makeplans"
	"github.com/gorilla/mux"
)

var ErrMissingID = errors.New("No id provided")

// TrainerProviders swagger:route GET /v1/trainers/providers user TrainerProviders
//
// Lists all providers for a Trainer
//
// Consumes:
// application/json
//
// Schemes: http, https
//
// Produces:
// application/json
//
// Responses:
// 200: default
func (s *Server) trainerProviders(w http.ResponseWriter, r *http.Request) {
	user, err := UserFromRequest(r)
	if isErrBadRequest(w, r, err) {
		return
	}
	db := FromRequest(r)
	fac, err := facade.NewTrainer(s.sch, db, user.ID)
	if isErrInternal(w, r, err) {
		return
	}

	switch r.Method {
	case "GET":
		provs, err := fac.Providers()
		if isErrInternal(w, r, err) {
			return
		}
		marshal(w, r, provs)
		return
	case "POST":
		req := types.TrainerProviderRequest{}
		bs, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		err = json.Unmarshal(bs, &req)
		if isErrBadRequest(w, r, err) {
			return
		}
		prov, err := fac.MakeProvider(req.ServiceID)
		if isErrInternal(w, r, err) {
			return
		}
		msg := fmt.Sprintf("successfully created provider: %d", prov.ID)
		marshal(w, r, msg)
		return
	}
}

func (s *Server) trainer(w http.ResponseWriter, r *http.Request) {
	tid := mux.Vars(r)["id"]
	if len(tid) == 0 {
		httpError(w, r, ErrMissingID.Error(), http.StatusBadRequest)
		return
	}

	db := FromRequest(r)
	fac, err := facade.NewTrainer(s.sch, db, tid)
	if isErrInternal(w, r, err) {
		return
	}
	t := fac.Get()

	if err == ErrMissingID {
		httpError(w, r, err.Error(), http.StatusBadRequest)
		return
	} else if isErrInternal(w, r, err) {
		return
	}

	marshal(w, r, t)
}

func (s *Server) trainerServices(w http.ResponseWriter, r *http.Request) {
	tid := mux.Vars(r)["id"]
	if len(tid) == 0 {
		httpError(w, r, ErrMissingID.Error(), http.StatusBadRequest)
		return
	}

	db := FromRequest(r)
	fac, err := facade.NewTrainer(s.sch, db, tid)
	if isErrInternal(w, r, err) {
		return
	}

	if r.Method == "GET" {
		svcs, err := fac.Services()
		if isErrInternal(w, r, err) {
			return
		}
		marshal(w, r, svcs)
		return
	}

	if r.Body == nil {
		httpError(w, r,
			`body must contain a service ie. {"service_id":"1"}`,
			http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	var svc makeplans.Service
	bs, err := ioutil.ReadAll(r.Body)
	if isErrBadRequest(w, r, err) {
		return
	}

	err = json.Unmarshal(bs, &svc)
	if isErrInternal(w, r, err) {
		return
	}

	if svc.ID == 0 {
		httpError(w, r, "service ID is required", http.StatusBadRequest)
		return
	}

	if r.Method != "POST" {
		errUnsupportedMethod(w, r)
		return
	}

	t := fac.Get()
	prv, err := s.sch.MakeProvider(makeplans.Provider{
		ServiceID:  svc.ID,
		ResourceID: t.Resource.ID,
	})
	if isErrInternal(w, r, err) {
		return
	}
	marshal(w, r, prv)

}

func (s *Server) trainerService(w http.ResponseWriter, r *http.Request) {
	tid := mux.Vars(r)["id"]
	if len(tid) == 0 {
		httpError(w, r, ErrMissingID.Error(), http.StatusBadRequest)
		return
	}

	db := FromRequest(r)
	fac, err := facade.NewTrainer(s.sch, db, tid)
	if isErrInternal(w, r, err) {
		return
	}

	sid := mux.Vars(r)["service_id"]
	if len(sid) == 0 {
		httpError(w, r, "service is required", http.StatusBadRequest)
		return
	}
	svcID, err := strconv.Atoi(sid)
	if isErrBadRequest(w, r, err) {
		return
	}

	svcs, err := fac.Services()
	if isErrInternal(w, r, err) {
		return
	}

	var matched []makeplans.Service
	for _, svc := range svcs {
		if svc.ID == svcID {
			matched = append(matched, svc)
		}
	}

	if len(matched) == 0 {
		marshal(w, r, Response{Error: "Service not found"})
		return
	}

	if r.Method == "GET" {
		marshal(w, r, matched[0])
		return
	}

	if r.Method != "DELETE" {
		errUnsupportedMethod(w, r)
		return
	}

	prvID, err := fac.DeleteTrainerService(svcID)
	if isErrInternal(w, r, err) {
		return
	}
	resp := Response{
		Success: fmt.Sprintf("deleted provider: %d", prvID),
	}
	marshal(w, r, resp)
}

func (s *Server) TrainerUpdate(w http.ResponseWriter, r *http.Request) {

	tid := mux.Vars(r)["id"]
	if len(tid) == 0 {
		httpError(w, r, ErrMissingID.Error(), http.StatusBadRequest)
		return
	}

	ctx := context.TODO()
	if r.Method != "PUT" {
		httpError(w, r, "Bad method only PUT supported", http.StatusBadRequest)
		return
	}

	if r.Body == nil {
		httpError(w, r, "No body received", http.StatusBadRequest)
		return
	}

	bs, _ := ioutil.ReadAll(r.Body)
	var req types.User
	err := json.Unmarshal(bs, &req)
	if err != nil {
		httpError(w, r, "Bad JSON", http.StatusBadRequest)
		return
	}

	if req.Role != types.TrainerRole {
		httpError(w, r, "role invalid, must be trainerrole", http.StatusBadRequest)
		return
	}

	db := FromRequest(r)
	fac, err := facade.NewTrainer(s.sch, db, tid)
	if isErrInternal(w, r, err) {
		return
	}
	trnr := fac.Get()

	var res makeplans.Resource
	if req.ResourceID > 0 {
		if trnr.ResourceID != req.ResourceID {
			httpError(w, r, "resource ID mismatch", http.StatusBadRequest)
			return
		}
		// Makeplans is more likely to fail, start with that
		res, err = s.sch.ResourceUpdate(*req.Resource)
		if err != nil {
			httpError(w, r, err.Error(), http.StatusBadRequest)
			return
		}
	}

	user, err := db.UpdateUser(ctx, req)
	if err != nil {
		httpError(w, r, err.Error(), http.StatusBadRequest)
		return
	}
	user.Resource = &res
	marshal(w, r, user)
}
