package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"golang.org/x/net/context"

	"github.com/drewwells/fitkiq/facade"
	"github.com/drewwells/fitkiq/types"
	"github.com/drewwells/makeplans"
	"github.com/gorilla/mux"
)

func (s *Server) setupCli(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	if cli := getHTTPCli(r); cli != nil {
		s.sch.HTTPCli = cli
	}
	if next != nil {
		next(w, r)
	}
}

// Bookings swagger:route GET /admin/bookings/ booking Bookings
//
// Bookings lists all the appointments that have been made on the site.
//
//
// Consumes:
// application/json
//
// Schemes: http, https
//
// Produces:
// application/json
//
// Responses:
// 200: BookingResponses
func (s *Server) bookings(w http.ResponseWriter, r *http.Request) {
	books, err := s.sch.Bookings(makeplans.BookingParams{})
	if err != nil {
		httpError(w, r, err.Error(), 500)
		return
	}
	marshal(w, r, books)
}

// BookingDelete swagger:route DELETE /admin/bookings/{id} booking BookingDelete
//
// Removes a booking
//
// Consumes:
// application/json
//
// Schemes: http, https
//
// Produces:
// application/json
//
// Responses:
// 200: BookingResponse
func (s *Server) bookingDelete(w http.ResponseWriter, r *http.Request) {
	sid := mux.Vars(r)["id"]
	id, err := strconv.Atoi(sid)
	if isErrBadRequest(w, r, err) {
		return
	}
	book, err := s.sch.BookingDelete(id)
	if err != nil {
		httpError(w, r, err.Error(), 500)
		return
	}
	marshal(w, r, Response{Success: fmt.Sprintf("deleted booking: %d", book.ID)})
}

func (s *Server) providers(w http.ResponseWriter, r *http.Request) {
	provs, err := s.sch.Providers()
	if err != nil {
		httpError(w, r, err.Error(), 500)
		return
	}
	marshal(w, r, provs)
}

func (s *Server) service(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		httpError(w, r, "http method not implemented",
			http.StatusInternalServerError)
		return
	}

	var svc makeplans.Service
	err := readBody(r.Body, &svc)
	if err != nil {
		httpError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}

	ret, err := s.sch.MakeService(svc)
	if err != nil {
		httpError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}
	marshal(w, r, ret)
}

func (s *Server) serviceMutate(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		httpError(w, r, err.Error(), http.StatusBadRequest)
		return
	}
	if id == 0 {
		httpError(w, r, "no id provided", http.StatusBadRequest)
		return
	}

	if r.Method == "DELETE" {
		_, err := s.sch.ServiceDelete(id)
		if err != nil {
			httpError(w, r, err.Error(), http.StatusInternalServerError)
			return
		}
		marshal(w, r, "deleted "+vars["id"])
		return
	}

	if r.Method != "PUT" {
		httpError(w, r, "not implemented", http.StatusBadRequest)
		return
	}

	var svc makeplans.Service
	err = readBody(r.Body, &svc)
	if err != nil {
		httpError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}

	ret, err := s.sch.ServiceUpdate(svc)
	if err != nil {
		httpError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}
	marshal(w, r, ret)
}

func (s *Server) resources(w http.ResponseWriter, r *http.Request) {
	res, err := s.sch.Resources()
	if err != nil {
		httpError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}
	marshal(w, r, res)
}

func (s *Server) resourceUpdate(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	if len(id) == 0 {
		httpError(w, r, "no id provided", http.StatusBadRequest)
		return
	}

	var req makeplans.Resource
	err := readBody(r.Body, &req)
	if err != nil {
		httpError(w, r, "PUT body not present", http.StatusBadRequest)
	}

	rsc, err := s.sch.ResourceUpdate(req)
	if err != nil {
		httpError(w, r, err.Error(), http.StatusBadRequest)
		return
	}
	marshal(w, r, rsc)
}

func (s *Server) resourceDelete(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	id := vars["id"]
	if len(id) == 0 {
		httpError(w, r, "no id provided", http.StatusBadRequest)
		return
	}

	i, err := strconv.Atoi(id)
	if err != nil {
		httpError(w, r, err.Error(), http.StatusBadRequest)
		return
	}

	rsc, err := s.sch.ResourceDelete(i)
	if err != nil {
		httpError(w, r, err.Error(), http.StatusBadRequest)
		return
	}
	marshal(w, r, rsc)
}

func (s *Server) provider(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		httpError(w, r, "http method not implemented",
			http.StatusInternalServerError)
		return
	}

	var prv makeplans.Provider
	err := readBody(r.Body, &prv)
	if err != nil {
		httpError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}

	ret, err := s.sch.MakeProvider(prv)
	if err != nil {
		httpError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}
	marshal(w, r, ret)
}

func (s *Server) providerMutate(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	if len(id) == 0 {
		httpError(w, r, "no id provided", http.StatusBadRequest)
		return
	}

	if r.Method == "DELETE" {
		i, err := strconv.Atoi(id)
		if err != nil {
			httpError(w, r, err.Error(), http.StatusBadRequest)
			return
		}
		rsc, err := s.sch.ProviderDelete(i)
		if err != nil {
			httpError(w, r, err.Error(), http.StatusBadRequest)
			return
		}
		marshal(w, r, rsc)
		return
	}

	httpError(w, r, "not implemented", http.StatusBadRequest)
}

func (s *Server) adminusers(w http.ResponseWriter, r *http.Request) {
	ctx := context.TODO()
	db := FromRequest(r)
	users, err := db.GetUsers(ctx, types.User{})
	if err != nil {
		httpError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}

	if len(users) == 0 {
		httpError(w, r, "no users found", http.StatusBadRequest)
		return
	}

	marshal(w, r, users)
}

func (s *Server) adminuser(w http.ResponseWriter, r *http.Request) {
	ctx := context.TODO()

	userid := mux.Vars(r)["user_id"]
	if len(userid) == 0 {
		httpError(w, r, "user id is required", http.StatusBadRequest)
		return
	}
	db := FromRequest(r)

	switch r.Method {
	case "DELETE":
		err := db.RemoveUser(ctx, userid)
		if isErrInternal(w, r, err) {
			return
		}
		marshal(w, r, "user deleted: "+userid)
		return
	case "PUT":
		var user types.User
		err := readBody(r.Body, &user)
		if isErrBadRequest(w, r, err) {
			return
		}

		u, err := db.UpdateUser(ctx, user)
		if isErrInternal(w, r, err) {
			return
		}
		u.Password = ""
		marshal(w, r, u)
	case "GET":
		u, err := db.GetUser(ctx, types.User{ID: userid})
		if isErrInternal(w, r, err) {
			return
		}
		u.Password = ""
		marshal(w, r, u)
	default:
		httpError(w, r, "method not implemented", http.StatusBadRequest)
	}

}

// AdminMakeBooking swagger:route POST /admin/bookings/ booking AdminMakeBooking
//
// Create a new booking. It must match an available slot on a service
//
//
// Consumes:
// application/json
//
// Schemes: http, https
//
// Produces:
// application/json
//
// Responses:
// 200: BookingResponse
func (s *Server) adminMakeBooking(w http.ResponseWriter, r *http.Request) {
	bs, err := ioutil.ReadAll(r.Body)
	if err != nil {
		httpError(w, r, "Invalid request body", http.StatusBadRequest)
		return
	}

	var b makeplans.Booking
	err = json.Unmarshal(bs, &b)
	if err != nil {
		httpError(w, r, err.Error(), http.StatusBadRequest)
		return
	}

	book, err := s.sch.MakeBooking(b)
	if err != nil {
		httpError(w, r, err.Error(), 500)
		return
	}
	marshal(w, r, types.BookingResponse{
		Booking: book,
	})
}

// Trainers swagger:route GET /admin/trainers/ trainer Trainers
//
// Gets available trainers
//
// Retrieves all the trainers
//
// Consumes:
// application/json
//
// Schemes: http, https
//
// Produces:
// application/json
//
// Responses:
// 200: UserResponse
func (s *Server) Trainers(w http.ResponseWriter, r *http.Request) {
	db := FromRequest(r)
	ts, err := facade.Trainers(db, s.sch, false)
	if err != nil {
		httpError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}
	marshal(w, r, ts)
}
