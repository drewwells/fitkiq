package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"golang.org/x/net/context"

	"github.com/drewwells/fitkiq/facade"
	"github.com/drewwells/fitkiq/types"
)

type fbauth struct {
	AccessToken    string `json:"accessToken"`
	UserID         string `json:"userID"`
	Expires        time.Time
	ExpiresSeconds int    `json:"expiresIn"`
	SignedRequest  string `json:"signedRequest"`
}

var (
	graphAPI   = "https://graph.facebook.com/v2.5/%s?fields=email,first_name,last_name,friends,id&access_token=%s"
	graphDebug = "https://graph.facebook.com/debug_token?input_token=%s&access_token=%s"
	//&amp;access_token={app-token-or-admin-token}"
	graphOauth = "https://graph.facebook.com/oauth/access_token?client_id=%s&client_secret=%s&grant_type=client_credentials"
)

var appID = "1626862994260188"

// FIXME: move this to an environment variable
var appSecret = "0e9afd33b7474b533796572e6c5cdfee"
var appToken string

// GET /oauth/access_token?
// client_id={app-id}
// &amp;client_secret={app-secret}
// &amp;grant_type=client_credentials
func getAppToken(r *http.Request) string {
	if len(appToken) > 0 {
		return appToken
	}
	cli := getHTTPCli(r)
	resp, err := cli.Get(fmt.Sprintf(graphOauth, appID, appSecret))
	if err != nil {
		log.Printf("failed to retrieve oauth token: %s", err)
		return ""
	}
	defer resp.Body.Close()
	fmt.Fscanf(resp.Body, "access_token=%s", &appToken)
	return appToken
}

func (s *Server) checkFB(w http.ResponseWriter, r *http.Request) {
	bs, err := ioutil.ReadAll(r.Body)
	if isErrBadRequest(w, r, err) {
		return
	}
	defer r.Body.Close()

	var auth types.FBAuth
	err = json.Unmarshal(bs, &auth)
	if isErrBadRequest(w, r, err) {
		return
	}

	db := FromRequest(r)

	fb, err := facade.NewAuth(s.sch, db)
	if isErrInternal(w, r, err) {
		return
	}

	ctx := types.NewContext(context.TODO(), r)

	user, err := fb.Facebook(ctx, auth)
	if isErrInternal(w, r, err) {
		return
	}

	user.Password = ""
	marshal(w, r, types.UserResponse{User: user})
}
