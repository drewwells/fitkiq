package store

import (
	"crypto/rand"
	"encoding/hex"
	"errors"

	"github.com/drewwells/fitkiq/store/adapter"
	"github.com/drewwells/fitkiq/types"

	"golang.org/x/crypto/bcrypt"
	"golang.org/x/net/context"
)

var (
	ErrUserNotFound = errors.New("User not found")
	ErrEmailTaken   = errors.New("email is registered")
	ErrEmailReqd    = errors.New("email is required")
)

type DB struct {
	store adapter.Store
}

func NewDB(store adapter.Store) *DB {
	return &DB{store: store}
}

func (db *DB) RemoveUser(ctx context.Context, userid string) error {
	if len(userid) == 0 {
		return ErrUserNotFound
	}
	return db.store.Delete(types.UserKind, []byte(userid))
}

func (db *DB) AddUser(ctx context.Context, u types.User) (types.User, error) {
	if len(u.Email) == 0 {
		return u, ErrEmailReqd
	}
	cnf := types.User{Email: u.Email}
	if confs, _ := db.GetUsers(ctx, cnf); len(confs) > 0 {
		return u, ErrEmailTaken
	}

	if len(u.Email) == 0 {
		return u, ErrEmailReqd
	}
	cnf = types.User{Email: u.Email}
	if confs, _ := db.GetUsers(ctx, cnf); len(confs) > 0 {
		return u, ErrEmailTaken
	}
	// Hash Password
	if len(u.Password) > 0 {
		pw, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
		if err != nil {
			return u, err
		}
		u.Password = string(pw)
	}

	// Build a token
	u.Token = generateToken()
	bid := []byte(u.ID)
	err := db.store.Put(types.UserKind, &bid, &u)
	u.ID = string(bid)
	return u, err
}

func generateToken() string {
	b := make([]byte, 8)
	rand.Read(b)
	return hex.EncodeToString(b)
}

func (db *DB) UpdateUser(ctx context.Context, u types.User) (ret types.User, err error) {
	id := u.ID
	exsts, err := db.GetUsers(ctx, types.User{ID: id})
	if err != nil {
		return
	}
	if len(exsts) == 0 {
		err = errors.New("user not found")
		return
	}

	exst := exsts[0]

	if len(u.FirstName) == 0 {
		u.FirstName = exst.FirstName
	}

	if len(u.LastName) == 0 {
		u.LastName = exst.LastName
	}

	if len(u.Email) == 0 {
		u.Email = exst.Email
	}

	if len(u.Password) == 0 {
		u.Password = exst.Password
	}

	if len(u.Description) == 0 {
		u.Description = exst.Description
	}

	if len(u.Token) == 0 {
		u.Token = exst.Token
	}

	if len(u.Role) == 0 {
		u.Role = exst.Role
	}

	if u.ResourceID == 0 {
		u.ResourceID = exst.ResourceID
	}

	if len(u.Picture) == 0 {
		u.Picture = exst.Picture
	}

	if u.Resource == nil {
		u.Resource = exst.Resource
	}

	if u.PersonID == 0 {
		u.PersonID = exst.PersonID
	}

	bid := []byte(id)
	err = db.store.Put(types.UserKind, &bid, &u)
	ret = u
	return
}

func (db *DB) GetUser(ctx context.Context, filter types.User) (ret types.User, err error) {
	users, err := db.GetUsers(ctx, filter)
	if err != nil {
		return
	}

	if len(users) != 1 {
		err = ErrUserNotFound
		return
	}

	ret = users[0]
	return
}

func (db *DB) GetUsers(ctx context.Context, filter types.User) ([]types.User, error) {

	if len(filter.ID) > 0 {
		var r types.User
		err := db.store.Get(types.UserKind, []byte(filter.ID), &r)
		if err != nil {
			return nil, err
		}
		// hacky but it works
		r.ID = filter.ID
		return []types.User{r}, err
	}

	var u types.User
	it, err := db.store.Cursor(types.UserKind)
	if err != nil {
		return nil, err
	}

	ret := make([]types.User, 0)

	for key, err := it.Next(&u); err != types.ErrDone; key, err = it.Next(&u) {
		if err != nil {
			return nil, err
		}
		if len(filter.Role) > 0 && filter.Role != u.Role {
			continue
		}
		if len(filter.FirstName) > 0 && filter.FirstName != u.FirstName {
			continue
		}
		if len(filter.LastName) > 0 && filter.LastName != u.LastName {
			continue
		}
		if len(filter.Email) > 0 && filter.Email != u.Email {
			continue
		}
		if len(filter.Password) > 0 {
			err := bcrypt.CompareHashAndPassword(
				[]byte(u.Password),
				[]byte(filter.Password),
			)
			if err != nil {
				continue
			}
		}
		if len(filter.Token) > 0 && filter.Token != u.Token {
			continue
		}
		u.ID = key
		ret = append(ret, u)
	}
	return ret, err
}
