// +build !appengine

package adapter

import (
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"errors"

	"github.com/boltdb/bolt"
	"github.com/drewwells/fitkiq/types"
	"golang.org/x/net/context"
)

var ErrNoBucket = errors.New("bucket does not exist")

var userBucket = []byte("user")

func createBucket(tx *bolt.Tx, bkt string) {
	tx.CreateBucketIfNotExists(userBucket)
}

var cheater Store
var defaultDBPath = "bolt.db"

func NewStore(cctx context.Context, path string) (Store, error) {
	// It's hard to not cheat
	if cheater != nil {
		return cheater, nil
	}
	if len(path) == 0 {
		path = defaultDBPath
	}
	db, err := bolt.Open(path, 0666, nil)
	if err != nil {
		return nil, err
	}
	cheater = &store{db: db}
	return cheater, err
}

type store struct {
	db *bolt.DB
}

func randBytes(i int) []byte {
	b := make([]byte, i)
	rand.Read(b)
	out := make([]byte, hex.EncodedLen(i))
	hex.Encode(out, b)
	return out
}

func (s *store) Put(kind string, key *[]byte, dst interface{}) error {
	return s.db.Update(func(tx *bolt.Tx) error {
		bkt := tx.Bucket([]byte(kind))
		if bkt == nil {
			createBucket(tx, kind)
			bkt = tx.Bucket([]byte(kind))
		}
		bs, err := json.Marshal(dst)
		if len(*key) == 0 {
			*key = randBytes(4)
		}
		if err != nil {
			return err
		}
		return bkt.Put(*key, bs)
	})
}

func (s *store) Delete(kind string, key []byte) error {
	return s.db.Update(func(tx *bolt.Tx) error {
		bkt := tx.Bucket([]byte(kind))
		if bkt == nil {
			return ErrNoBucket
		}
		return bkt.Delete(key)
	})
}

func (s *store) Get(kind string, key []byte, dst interface{}) error {
	return s.db.View(func(tx *bolt.Tx) error {
		bkt := tx.Bucket([]byte(kind))
		if bkt == nil {
			return ErrNoBucket
		}
		val := bkt.Get(key)
		return json.Unmarshal(val, dst)
	})
}

func (s *store) Cursor(kind string) (Cursor, error) {
	tx, err := s.db.Begin(false)
	if err != nil {
		return nil, err
	}
	c := cursor{tx: tx}
	bkt := tx.Bucket([]byte(kind))
	if bkt == nil {
		tx.Rollback()
		return nil, ErrNoBucket
	}
	c.cur = bkt.Cursor()
	return &c, err
}

type cursor struct {
	tx     *bolt.Tx
	cur    *bolt.Cursor
	opened bool
}

func (c *cursor) Next(dst interface{}) (string, error) {
	var k, v []byte
	if !c.opened {
		c.opened = true
		k, v = c.cur.First()
	} else {
		k, v = c.cur.Next()
	}

	if k == nil {
		// We're only allowing read so maybe Rollback is faster?
		c.tx.Rollback()
		return "", types.ErrDone
	}
	err := json.Unmarshal(v, dst)
	return string(k), err
}
