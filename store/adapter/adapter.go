package adapter

import (
	"errors"

	"github.com/drewwells/fitkiq/types"

	"golang.org/x/net/context"
)

var ErrNoCtx = errors.New("appengine context not found")

type Store interface {
	Delete(kind string, key []byte) error
	Put(kind string, key *[]byte, dst interface{}) error
	Get(kind string, key []byte, dst interface{}) error
	// Send the kind and type of objects being fetched
	Cursor(kind string) (Cursor, error)
}

type Query interface {
	Run(c types.Context) Iterator
}

type Iterator interface {
	Next(interface{}) (string, error)
}

type Cursor interface {
	Next(dst interface{}) (key string, err error)
}

type key int

const (
	storeKey key = iota
)

func WithStore(c context.Context, store Store) context.Context {
	return context.WithValue(c, storeKey, store)
}
