// +build appengine

package adapter

import (
	"github.com/drewwells/fitkiq/types"
	"golang.org/x/net/context"

	"google.golang.org/appengine/datastore"
)

func NewStore(cctx context.Context, _ string) (Store, error) {
	ctx, ok := types.FromContext(cctx).(context.Context)
	if !ok {
		return nil, ErrNoCtx
	}
	return gaeStore{c: ctx}, nil
}

type gaeStore struct {
	c context.Context
}

func (store gaeStore) Delete(kind string, key []byte) error {
	// TODO: add this
	sKey := string(key)
	gaeKey := datastore.NewKey(store.c, kind, sKey, 0, nil)
	return datastore.Delete(store.c, gaeKey)
}

func (store gaeStore) Put(kind string, key *[]byte, dst interface{}) error {
	var err error
	sKey := string(*key)
	gaeKey := datastore.NewKey(store.c, kind, sKey, 0, nil)
	gaeKey, err = datastore.Put(store.c, gaeKey, dst)
	*key = []byte(gaeKey.Encode())
	return err
}

func (store gaeStore) Get(kind string, key []byte, dst interface{}) error {
	sKey := string(key)
	gaeKey, err := datastore.DecodeKey(sKey)
	if err != nil {
		return err
	}
	return datastore.Get(store.c, gaeKey, dst)
}

func (s gaeStore) Cursor(kind string) (Cursor, error) {
	qry := datastore.NewQuery(kind)
	cur := gaeCursor{
		query:    qry,
		iterator: qry.Run(s.c),
	}
	return cur, nil
}

type gaeCursor struct {
	query    *datastore.Query
	iterator *datastore.Iterator
}

func (r gaeCursor) Next(dst interface{}) (string, error) {
	key, err := r.iterator.Next(dst)
	if err != nil {
		if err == datastore.Done {
			return "", types.ErrDone
		}
		return "", err
	}
	return key.Encode(), err
}
