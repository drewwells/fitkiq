package store

import (
	"testing"

	"github.com/drewwells/fitkiq/types"

	"golang.org/x/net/context"

	"appengine/aetest"
	"appengine/datastore"
)

func TestUser_get(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}
	defer c.Close()
	ctx := types.NewContext(context.TODO(), c)

	key := datastore.NewKey(c, types.UserKind, "", 0, nil)
	in := types.User{
		FirstName: "first",
		LastName:  "last",
		Email:     "me@email.com",
		Password:  "pass",
		Token:     "toke",
	}
	if key, err = datastore.Put(c, key, &in); err != nil {
		t.Fatal(err)
	}

	// Umm this is req'd, b/c NewQuery won't see the Put done above
	datastore.Get(c, key, &types.User{})

	rets, err := GetUsers(ctx, types.User{})
	if err != nil {
		t.Fatal(err)
	}

	if e := 1; len(rets) != e {
		t.Fatalf("got: %d wanted: %d", len(rets), e)
	}
	r := rets[0]
	if e := "first"; e != r.FirstName {
		t.Errorf("got: %s, wanted: %s", r.FirstName, e)
	}
}

func TestUser_conflict(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}
	defer c.Close()
	ctx := types.NewContext(context.TODO(), c)

	key := datastore.NewKey(c, types.UserKind, "", 0, nil)
	in := types.User{
		FirstName: "first",
		LastName:  "last",
		Email:     "email@me.com",
		Password:  "pass",
		Token:     "toke",
	}
	if key, err = datastore.Put(c, key, &in); err != nil {
		t.Fatal(err)
	}

	err = AddUser(ctx, types.User{})
	if err != ErrEmailReqd {
		t.Fatal(err)
	}

	datastore.Get(c, key, &types.User{})
	if err = AddUser(ctx, types.User{Email: "email@me.com"}); err != ErrEmailTaken {
		t.Fatal(err)
	}

}
