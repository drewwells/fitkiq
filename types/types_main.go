// +build !appengine

package types

import (
	"net/http"

	"golang.org/x/net/context"
)

func init() {
	NewContext = func(ctx context.Context, r *http.Request) context.Context {
		return ctx
	}

	ClientFromCtx = func(ctx context.Context) *http.Client {
		return &http.Client{}
	}
}
