package types

import "github.com/drewwells/makeplans"

// Types required for Swagger to work

// SlotResponse responds with the the available time slot for a booking
//
// swagger:response SlotResponse
type SlotResponse struct {
	// in: body
	Slot makeplans.Slot
}

// UserRequest
//
// swagger:parameters Signup
type UserRequest struct {
	// User to submit
	//
	// in: body
	User User `json:"user"`
}

// UserResponse
//
// swagger:response UserResponse
type UserResponse struct {
	// User object
	//
	// in: body
	User User `json:"user"`
}

// SlotRequest is
//
// This is used for operations that want an Order as body of the request
// swagger:parameters ServiceSlot
type SlotRequest struct {
	// ID of the service
	//
	// in: path
	// required: true
	ID string `json:"id"`
	// Starting time to query for slots
	//
	// in: url
	// required: false
	From string `json:"from"`
	// Ending time to query for slots
	//
	// in: url
	// required: false
	To string `json:"to"`
	// Resource (trainers) to limit results to comma separate
	// ?res=484,501
	//
	// in: url
	// required: false
	Res string `json:"res"`
}

// BookingMutate
//
// swagger:parameters BookingDelete
type BookingMutate struct {
	// ID of the booking
	//
	// in: path
	// required: true
	ID string `json:"id"`
}

// BookingResponses is a slice of bookings
//
// swagger:response BookingResponses
type BookingResponses struct {
	// Booking slice
	//
	// in: body
	// required: true
	Bookings []makeplans.Booking `json:"bookings"`
}

// BookingResponse represents a scheduled appointment
//
// swagger:response BookingResponse
type BookingResponse struct {
	// Booking struct
	//
	// in: body
	// required: true
	Booking makeplans.Booking `json:"booking"`
}

// Booking represents a scheduled appointment
//
// swagger:parameters MakeBooking
type BookingRequest struct {
	// Booking to be created
	//
	// in: body
	// required: true
	Booking makeplans.Booking
}
