package types

import "github.com/drewwells/makeplans"

var UserKind string = "user"

var TrainerRole = "trainer"
var AdminRole = "admin"
var MemberRole = "member"

// User is an actor in the system. User can have 3 different roles
// admin, trainer, client
//
type User struct {
	ID           string              `json:"id"`
	FirstName    string              `json:"first_name"`
	LastName     string              `json:"last_name"`
	Email        string              `json:"email,omitempty"`
	Password     string              `json:"password,omitempty"`
	Description  string              `json:"description,omitempty"`
	Token        string              `json:"token,omitempty"`
	Role         string              `json:"role"`
	ResourceID   int                 `json:"resource_id,omitempty"`
	PersonID     int                 `json:"person_id,omitempty"`
	Resource     *makeplans.Resource `json:"resource,omitempty"`
	Picture      string              `json:"picture"`
	Certificates []Certificate       `json:"certificates,omitempty"`
}

// Certificate describes trainers credentials
type Certificate struct{}
