package types

// TrainerProviderRequest is used to create a provider for a trainer
type TrainerProviderRequest struct {
	ServiceID int `json:"service_id"`
}
