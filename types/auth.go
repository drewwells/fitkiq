package types

import "time"

type FBAuth struct {
	AccessToken    string `json:"accessToken"`
	UserID         string `json:"userID"`
	Expires        time.Time
	ExpiresSeconds int    `json:"expiresIn"`
	SignedRequest  string `json:"signedRequest"`
}
