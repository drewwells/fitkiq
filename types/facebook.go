package types

// Types specific to Facebook API

type DebugToken struct {
	AppID       string   `json:"app_id"`
	Application string   `json:"application"`
	ExpiresAt   int      `json:"expires_at"`
	IsValid     bool     `json:"is_valid"`
	Scopes      []string `json:"scopes"`
	UserID      string   `json:"user_id"`
}

type GraphProfile struct {
	ID        string       `json:"id"`
	FirstName string       `json:"first_name"`
	LastName  string       `json:"last_name"`
	Friends   GraphFriends `json:"friends"`
	Email     string       `json:"email"`
}

type GraphFriendsSummary struct {
	TotalCount int `json:"total_count"`
}

type GraphFriendsData struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type GraphFriends struct {
	Data    []GraphFriendsData  `json:"data"`
	Summary GraphFriendsSummary `json:"summary"`
}
