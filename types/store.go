package types

import "errors"

var ErrDone = errors.New("iterator exhausted")
