// +build appengine

package types

import (
	"net/http"

	"golang.org/x/net/context"

	"google.golang.org/appengine"
	"google.golang.org/appengine/urlfetch"
)

func init() {
	NewContext = func(ctx context.Context, r *http.Request) context.Context {
		c := appengine.NewContext(r)
		return context.WithValue(ctx, appCtxKey, c)
	}

	ClientFromCtx = func(ctx context.Context) *http.Client {
		cli := &http.Client{
			Transport: &urlfetch.Transport{
				Context: ctx,
				AllowInvalidServerCertificate: true,
			}}
		return cli
	}
}
