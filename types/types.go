package types

import (
	"net/http"

	"golang.org/x/net/context"
)

type key int

const (
	appCtxKey key = iota
)

type Context interface{}

var NewContext func(context.Context, *http.Request) context.Context

func FromContext(ctx context.Context) interface{} {
	return ctx.Value(appCtxKey)
}

var ClientFromCtx func(context.Context) *http.Client
