package api

import (
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

// Response is the default HTTP response for all routes
//
// swagger:response response
type Response struct {
	Error   string `json:"error,omitempty"`
	Success string `json:"success,omitempty"`
}

func marshal(w http.ResponseWriter, r *http.Request, v interface{}) {
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "true")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	bs, err := json.MarshalIndent(v, "  ", "    ")
	if err != nil {
		httpError(w, r, err.Error(), http.StatusInternalServerError)
	}
	w.Write(bs)
}

func httpError(w http.ResponseWriter, r *http.Request, s string, code int) {
	bs, _ := json.Marshal(Response{Error: s})
	http.Error(w, string(bs), code)
}

func errUnsupportedMethod(w http.ResponseWriter, r *http.Request) {
	httpError(w, r, "unsupported method", http.StatusBadRequest)
}

func isErrBadRequest(w http.ResponseWriter, r *http.Request, err error) bool {
	return isErr(w, r, err, http.StatusBadRequest)
}

func isErrInternal(w http.ResponseWriter, r *http.Request, err error) bool {
	return isErr(w, r, err, http.StatusInternalServerError)
}

func isErr(w http.ResponseWriter, r *http.Request, err error, code int) bool {
	if err != nil {
		log.Println("error", err)
		httpError(w, r, err.Error(), code)
		return true
	}
	return false
}

func readBody(r io.ReadCloser, v interface{}) error {
	if r == nil {
		return errors.New("request body is empty")
	}
	defer r.Close()
	bs, err := ioutil.ReadAll(r)
	if err != nil {
		return err
	}
	return json.Unmarshal(bs, v)
}

func cors(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {

	origin := r.Header.Get("Origin")

	w.Header().Add("Access-Control-Allow-Origin", origin)
	w.Header().Add("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS")
	w.Header().Add("Access-Control-Allow-Headers", "Content-Type, Authorization")
	w.Header().Add("Content-Type", "application/json")

	// Don't process OPTIONS any furtherreadbo
	if r.Method == "OPTIONS" {
		return
	}

	if next != nil {
		next(w, r)
	}
}
